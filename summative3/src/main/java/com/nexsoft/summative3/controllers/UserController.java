package com.nexsoft.summative3.controllers;


import javax.validation.Valid;

import com.nexsoft.summative3.models.entities.User;
import com.nexsoft.summative3.models.form.LoginForm;
import com.nexsoft.summative3.models.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Controller
public class UserController implements WebMvcConfigurer {

    @Autowired
    UserService userService;

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

    @PostMapping("/login/signin")
    public String signIn(Model model, LoginForm loginForm) {

        if (userService.findByEmail(loginForm.getUsername()) == null
                && userService.findByPhoneNumber(loginForm.getUsername()) == null) {
            model.addAttribute("error", "Email / Phone Number Tidak Terdaftar!");
            return "login";
        }

        User user = new User();

        if (userService.findByEmail(loginForm.getUsername()) != null) {
            user = userService.findByEmail(loginForm.getUsername());
        }

        if (userService.findByPhoneNumber(loginForm.getUsername()) != null) {
            user = userService.findByPhoneNumber(loginForm.getUsername());
        }

        if (!user.getPassword().equals(loginForm.getPassword())) {
            model.addAttribute("error", "Password Salah!");
            return "login";
        }

        return "welcome";

    }

    @GetMapping("/register")
    public String showRegister() {

        return "register";
    }

    @PostMapping("/register/create")
    public String checkRegisterForm(@Valid User user, BindingResult bindingResult, Model model) {

        // Check Password Match
        if (!user.getPassword().equals(user.getCpassword())) {
            model.addAttribute("error", "Password tidak sesuai");
            return "register";
        }

        if (userService.findByEmail(user.getEmail()) != null) {
            model.addAttribute("error", "Email telah terdaftar");
            return "register";
        }

        if (userService.findByPhoneNumber(user.getPhoneNumber()) != null) {
            model.addAttribute("error", "Phonenumber telah terdaftar");
            return "register";
        }

        String phone = user.getPhoneNumber();

        if(phone.substring(0,2).matches("62")){
            user.setPhoneNumber("+".concat(phone));
        }

        if (phone.matches("0.*[0-9]")) {
            user.setPhoneNumber("+62".concat(phone.substring(1)) );
        }



        userService.save(user);
        return "redirect:/login";

    }

}

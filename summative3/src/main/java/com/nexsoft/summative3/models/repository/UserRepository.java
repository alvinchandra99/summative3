package com.nexsoft.summative3.models.repository;


import com.nexsoft.summative3.models.entities.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

   
    public User save(User user);
    public User findByEmail(String email);
    public User findByPhoneNumber(String phoneNumber);
    
}

package com.nexsoft.summative3.models.service;


import javax.transaction.Transactional;

import com.nexsoft.summative3.models.entities.User;
import com.nexsoft.summative3.models.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User save(User user) {
        return userRepository.save(user);
    }

    public User findByEmail (String email) {
        return userRepository.findByEmail(email);
        
    }

    public User findByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneNumber(phoneNumber);
    }

}

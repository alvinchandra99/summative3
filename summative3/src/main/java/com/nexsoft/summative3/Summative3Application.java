package com.nexsoft.summative3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Summative3Application {

	public static void main(String[] args) {
		SpringApplication.run(Summative3Application.class, args);
	}

}
